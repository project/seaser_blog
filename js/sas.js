(function ($) {
  'use strict';

  Drupal.behaviors.awesome = {
    attach: function(context, settings) {
      $('.node-readmore a', context).once('buton').addClass('uk-button uk-button-default uk-float-right');
      $('.search-block-form form input', context).once('aramaKutu').addClass('uk-search-input');
      $('.search-block-form form input', context).once('aramaKutuAttr').attr({autofocus: "true", placeholder: "search..."});
      $('.search-block-form form input.button', context).once('aramabuton').addClass('uk-hidden');
      $('.uk-article > div > p:first-child', context).once('dropcap').addClass('uk-dropcap');
    }
  };

}(jQuery));